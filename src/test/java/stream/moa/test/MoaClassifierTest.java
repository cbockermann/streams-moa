/**
 * 
 */
package stream.moa.test;

import java.net.URL;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.CsvStream;
import stream.io.SourceURL;
import stream.io.Stream;
import stream.moa.MoaObjectFactory;
import stream.runtime.ProcessContainer;
import stream.runtime.setup.factory.ObjectFactory;

/**
 * @author chris
 * 
 */
public class MoaClassifierTest {

	static Logger log = LoggerFactory.getLogger(MoaClassifierTest.class);

	@Test
	public void test() throws Exception {

		ObjectFactory.registerObjectCreator(new MoaObjectFactory());

		URL url = MoaClassifierTest.class.getResource("/moa-test.xml");
		ProcessContainer pc = new ProcessContainer(url);
		pc.run();
	}

	public static void main(String[] args) throws Exception {

		Stream stream = new CsvStream(
				new SourceURL("classpath:/multi-golf.csv"));
		stream.init();

		Data item = stream.read();
		while (item != null) {
			log.info("item: {}", item);
			item = stream.read();
		}

		new MoaClassifierTest().test();
	}
}
