/**
 * 
 */
package stream.moa.test;

import static org.junit.Assert.fail;

import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.moa.MoaParameterFinder;

/**
 * @author chris
 * 
 */
public class MoaParameterFinderTest {

	static Logger log = LoggerFactory.getLogger(MoaParameterFinderTest.class);

	/**
	 * Test method for
	 * {@link stream.moa.MoaParameterFinder#findParameters(java.lang.Class)}.
	 */
	@Test
	public void testFindParameters() {

		try {
			String className = "moa.classifiers.bayes.NaiveBayes";
			Class<?> clazz = Class.forName(className);

			log.info("Checking class {}", clazz);

			ClassLoader cl = MoaParameterFinder.class.getClassLoader();
			log.info("Using classloader: {}", cl);

			Map<String, Class<?>> pt = MoaParameterFinder.findParams(clazz);
			log.info("Found {} parameters.", pt.size());

		} catch (Exception e) {
			e.printStackTrace();
			fail("error: " + e.getMessage());
		}
	}
}
