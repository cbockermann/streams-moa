package stream.moa.test;

import java.net.URL;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.moa.MoaObjectFactory;
import stream.runtime.ProcessContainer;
import stream.runtime.setup.factory.ObjectFactory;

public class MoaOutlierTest {

	static Logger log = LoggerFactory.getLogger(MoaOutlierTest.class);

	@Test
	public void test() throws Exception {

		ObjectFactory.registerObjectCreator(new MoaObjectFactory());

		URL url = MoaClassifierTest.class.getResource("/moa-outlier-test.xml");
		ProcessContainer pc = new ProcessContainer(url);
		pc.run();
	}

	public static void main(String[] args) throws Exception {
		new MoaOutlierTest().test();
	}
}
