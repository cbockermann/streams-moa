/**
 * 
 */
package stream.learner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;

import net.minidev.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.data.Statistics;
import stream.io.Sink;

/**
 * @author chris
 * 
 */
public class PredictionError extends AbstractProcessor {

	static Logger logger = LoggerFactory.getLogger(PredictionError.class);
	Statistics statistics = new Statistics();
	Long tests = 0L;
	String[] classes;

	String id = null;
	ArrayList<String> labels = new ArrayList<String>();
	Sink[] output;
	File log;

	String predictionKey = "prediction(@label)";

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {

		super.init(ctx);

		if (classes != null) {
			for (String cls : classes) {
				if (!labels.contains(cls)) {
					labels.add(cls);
				}
			}

			if (!labels.isEmpty()) {
				// positiveClass = labels.get(0);
				// negativeClass = "!" + positiveClass;
			}
		}
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		if (input.containsKey("@label") && input.containsKey(predictionKey)) {
			String label = input.get("@label").toString();

			if (!labels.contains(label)) {
				logger.debug("Found new class '{}'", label);
				if (labels.isEmpty()) {
					logger.debug("Using class '{}' as positive class.", label);
				}
				labels.add(label);
			}

			String positiveClass = label;
			String negativeClass = "!" + label;

			String pred = input.get(predictionKey).toString();

			if (pred.equals(label)) {
				input.put("@error", "no");
				statistics.add("prediction.correct", 1.0);
			} else {
				input.put("@error", "yes");
				statistics.add("prediction.wrong", 1.0);
			}

			statistics.add(label + ".total", 1.0);
			statistics.add(label + ".predicted." + pred, 1.0);

			Double tp = value(positiveClass + ".predicted." + positiveClass);
			Double fp = value(positiveClass + ".predicted." + negativeClass);
			Double tn = value(negativeClass + ".predicted." + negativeClass);
			Double fn = value(negativeClass + ".predicted." + positiveClass);

			Double p = value(positiveClass + ".total");
			if (p == null)
				p = 0.0;
			Double n = value(negativeClass + ".total");
			if (n == null)
				n = 0.0;

			Double acc = (tp + tn) / (p + n);
			input.put("@accuracy", acc);
			input.put("@precision", (tp / (fp + tp)));
			input.put("@f-score", (2 + tp) / (2 * tp + fp + fn));

			tests++;
		}

		return input;
	}

	/**
	 * @see stream.AbstractProcessor#finish()
	 */
	@Override
	public void finish() throws Exception {
		super.finish();

		for (String positiveClass : this.labels) {

			for (String negativeClass : this.labels) {
				if (positiveClass.equals(negativeClass)) {
					continue;
				}
				Double tp = value(positiveClass + ".predicted." + positiveClass);
				Double fp = value(negativeClass + ".predicted." + positiveClass);
				Double tn = value(negativeClass + ".predicted." + negativeClass);
				Double fn = value(positiveClass + ".predicted." + negativeClass);

				Double p = value(positiveClass + ".total");
				if (p == null)
					p = 0.0;
				Double n = value(negativeClass + ".total");
				if (n == null)
					n = 0.0;

				Double acc = (tp + tn) / (p + n);
				Double precision = (tp / (fp + tp));
				Double fScore = (2 + tp) / (2 * tp + fp + fn);
				Double qfactor = qfactor(tp, fp, tn, fn);

				logger.info("+---------------------------------------------------------");
				logger.info(
						"|  Prediction Error (ID '{}', {} elements tested.)",
						id, tests);
				logger.info("|    true '{}'  {}", positiveClass,
						value(positiveClass + ".total"));
				logger.info("|    true '{}'  {}", negativeClass,
						value(negativeClass + ".total"));

				logger.info("|");
				logger.info("|  accuracy:  {}", acc);
				logger.info("|  precision: {}", precision);
				logger.info("|  f-score:   {}", fScore);

				logger.info("|");

				Collections.sort(labels);
				for (String l : labels) {
					Double correct = value(l + ".predicted." + l);
					Double wrong = 0.0;
					for (String cl : labels) {
						if (l.equals(cl)) {
							continue;
						}
						wrong += value(l + ".predicted." + cl);
					}
					logger.info("|   {} correct: {}", l, correct);
					logger.info("|   {} wrong: {}", l, wrong);
				}
				logger.info("|");
				for (String k : statistics.keySet()) {

					if (k.indexOf(".predicted.") > 0) {
						logger.info("|    {} => {}", k, statistics.get(k));
					}
				}
				logger.info("|");
				logger.info("|  true-positive-rate  (positive={}) : {}",
						positiveClass);
				logger.info("+---------------------------------------------------------");

				if (log != null) {
					try {

						statistics.put("accuracy", acc);
						statistics.put("precision", precision);
						statistics.put("fScore", fScore);
						statistics.put("qfactor", qfactor);

						logger.info("Writing performance log to {}", log);
						// boolean header = !log.isFile() || log.length() == 0L;
						FileOutputStream fos = new FileOutputStream(log, true);
						PrintStream ps = new PrintStream(fos);
						ps.println(JSONObject.toJSONString(statistics));
						ps.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	protected static double qfactor(double tp, double fp, double tn, double fn) {
		return (tp / (tp + fp)) / Math.sqrt(fp / (fp + tn));
	}

	public Double value(String key) {
		Double d = statistics.get(key);
		if (d == null) {
			return 0.0;
		}
		return d;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the output
	 */
	public Sink[] getOutput() {
		return output;
	}

	/**
	 * @param output
	 *            the output to set
	 */
	public void setOutput(Sink[] output) {
		this.output = output;
	}

	/**
	 * @return the log
	 */
	public File getLog() {
		return log;
	}

	/**
	 * @param log
	 *            the log to set
	 */
	public void setLog(File log) {
		this.log = log;
	}

	/**
	 * @return the predictionKey
	 */
	public String getPredictionKey() {
		return predictionKey;
	}

	/**
	 * @param predictionKey
	 *            the predictionKey to set
	 */
	public void setPredictionKey(String predictionKey) {
		this.predictionKey = predictionKey;
	}

}
