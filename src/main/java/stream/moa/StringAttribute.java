/**
 * 
 */
package stream.moa;

import java.util.ArrayList;

import weka.core.Attribute;

/**
 * This class implements an attribute of type <i>String</i>. This is the same as
 * the standard WEKA implementation of String attributes, except, that it allows
 * for a specified string value to be put onto index 0, instead of the default
 * WEKA dummy string value.
 * 
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 * 
 */
public class StringAttribute extends Attribute {

	/** The unique class ID */
	private static final long serialVersionUID = -6478741288493392903L;

	/**
	 * @param attributeName
	 * @param attributeValues
	 */
	private StringAttribute(String attributeName) {
		super(attributeName, new ArrayList<String>());
		this.m_Type = Attribute.STRING;
	}

	public StringAttribute(String name, String firstValue) {
		this(name);
		this.addStringValue(firstValue);
	}
}