/**
 *
 */
package stream.moa;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

import moa.classifiers.AbstractClassifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.Measurable;
import stream.ProcessContext;
import stream.io.SourceURL;
import stream.runtime.setup.ParameterInjection;
import stream.util.Variables;
import weka.core.Instance;

/**
 * <p>
 * This class is a generic wrapper for MOA operators.
 * </p>
 *
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 *
 */
public abstract class MoaProcessor extends AbstractProcessor implements Measurable {

	static Logger log = LoggerFactory.getLogger(MoaProcessor.class);
//	final Class<?> moaClass;
//	AbstractClassifier classifier;

	String label = "@label";
	String[] features = new String[] { "*" };
	String[] classes = null;

	protected DataInstanceWrapper instanceWrapper;
	String id;

	SourceURL model;
	MoaModel moaModel;

//	public MoaProcessor(Class<?> moaClass) throws ClassNotFoundException,
//			InstantiationException, IllegalAccessException {
//		this.moaClass = moaClass;
//
//		log.debug("Creating new MoaProcessor for class {}", moaClass);
//		classifier = (AbstractClassifier) moaClass.newInstance();
//
//		log.debug("MOA object is {}", classifier);
//	}

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

//		classifier.prepareForUse();

		//
		// create data-instance-wrapper
		//
		if (instanceWrapper == null) {
			log.info(
					"Creating data-instance wrapper, label = '{}', features = '{}'",
					label, features);
			instanceWrapper = new DataInstanceWrapper(label, features, classes);
		}

//		this.moaModel = new MoaModel(classifier, instanceWrapper);
	}

//	public void setParameters(Map<String, String> params) throws Exception {
//		MoaParameterFinder.injectParams(params, classifier);
//
//		if (params.containsKey("id")) {
//			setId(params.get("id"));
//		}
//		ParameterInjection.inject(this, params, new Variables());
//		ParameterInjection.inject(classifier, params, new Variables());
//	}
//
//	public Map<String, Class<?>> getParameters() throws Exception {
//		return MoaParameterFinder.findParams(classifier.getClass());
//	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @see stream.Processor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data input) {
		DataInstance instance = wrap(input);
		processInstance(instance);
		return input;
	}

	public DataInstance wrap(Data input) {
//		return moaModel.wrap(input);
		return this.instanceWrapper.wrap(input);
	}

	public void processInstance(Instance instance) {
	}

	/**
	 * @see stream.AbstractProcessor#finish()
	 */
	@Override
	public void finish() throws Exception {
		super.finish();
		log.info("Finishing MoaProcessor...");
		log.info("Model location is {}", model);

		if (model != null) {
			File file = new File(model.getPath());
			log.info("Storing trained model in file {}", file);

			FileOutputStream fos = new FileOutputStream(file);
			MoaModel.write(this.moaModel, fos);
			fos.close();
		}

	}


	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the features
	 */
	public String[] getFeatures() {
		return features;
	}

	/**
	 * @param features
	 *            the features to set
	 */
	public void setFeatures(String[] features) {
		this.features = features;
	}

	/**
	 * @return the classes
	 */
	public String[] getClasses() {
		return classes;
	}

	/**
	 * @param classes
	 *            the classes to set
	 */
	public void setClasses(String[] classes) {
		this.classes = classes;
	}

	/**
	 * @return the model
	 */
	public SourceURL getOutput() {
		return model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setOutput(SourceURL model) {
		this.model = model;
	}
}