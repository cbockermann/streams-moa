package stream.moa;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Set;

import moa.classifiers.AbstractClassifier;
import moa.clusterers.outliers.MyBaseOutlierDetector;
import moa.clusterers.outliers.MyBaseOutlierDetector.Outlier;
import moa.core.InstancesHeader;
import moa.options.OptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.ProcessContext;
import stream.moa.MoaClassifier;
import stream.moa.MoaProcessor;
import weka.core.Attribute;
import weka.core.Instance;

/**
 * Wrapper for MOA class MyBaseOutlierDetector.
 *
 * @author roetner
 *
 */
public class MoaOutlierDetector extends MoaProcessor {

	static Logger log = LoggerFactory.getLogger(MoaClassifier.class);
	protected Object lock = new Object();
	protected MyBaseOutlierDetector moaOutlierDetector;

	public MoaOutlierDetector(OptionHandler opt){
		if (opt instanceof MyBaseOutlierDetector) {
			moaOutlierDetector = (MyBaseOutlierDetector) opt;
			log.debug("Creating new MoaOutlierDetector for class {}", opt.getClass().getName());
		} else
			log.debug("Creating MoaOutlierDetector failed: " + opt.getClass().getName() + "does not implement MyBaseOutlierDetector");
	}

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);
		getOutlierDetector().prepareForUse();
		getOutlierDetector().resetLearning();
	}


	/**
	 * @see stream.moa.MoaProcessor#process(weka.core.Instance)
	 */
	@Override
	public void processInstance(Instance instance) {
		synchronized (lock) {
//			if (moaOutlierDetector.getModelContext()==null) {
//				getOutlierDetector().setModelContext(new InstancesHeader(instance.dataset()));
//			}
			getOutlierDetector().trainOnInstance(instance);
		}
	}

//	/*/** ATM there is no Service for OutlierDetection...
//	 * @see stream.service.Service#reset()
//	 *
//	@Override*/
//	public void reset() throws Exception {
//		getOutlierDetector().resetLearning();
//	}


	@Override
	public double getByteSize() {
		return getOutlierDetector().measureByteSize();
	}

    protected MyBaseOutlierDetector getOutlierDetector() {
    	return moaOutlierDetector;
    }

	/**
	 * @see stream.AbstractProcessor#finish()
	 */
	@Override
	public void finish() throws Exception {
		super.finish();
		if (moaOutlierDetector!=null) {
			Set<Outlier> outliers= moaOutlierDetector.GetOutliersFound();
	        StringBuffer str = new StringBuffer("\n\n OutlierDetection: Results from ");
	        str.append(this.getId());
	        str.append("\n Number of Outliers Detected: ");
	        str.append(outliers.size());
//	        str.append("\n Detected Outliers: ");
//			for (Outlier o : outliers) {
//	            str.append(o.id);
//	            str.append(" , ");
//	        }
			str.append("\n\n");
		log.info(str.toString());
		}

	}

}
