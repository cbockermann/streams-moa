/**
 * 
 */
package stream.moa;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import moa.classifiers.AbstractClassifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

/**
 * @author chris
 * 
 */
public class MoaModel {

	static Logger log = LoggerFactory.getLogger(MoaModel.class);
	AbstractClassifier classifier;
	DataInstanceWrapper wrapper;

	protected MoaModel(AbstractClassifier c, DataInstanceWrapper w) {
		this.classifier = c;
		this.wrapper = w;
	}

	public static MoaModel read(InputStream in) throws Exception {
		log.info("Reading MOA model from {}", in);
		ObjectInputStream ois = new ObjectInputStream(in);

		AbstractClassifier classifier = (AbstractClassifier) ois.readObject();
		log.info("model.classifier is: {}", classifier);

		DataInstanceWrapper wrapper = (DataInstanceWrapper) ois.readObject();
		log.info("model.wrapper is: {}", wrapper);

		classifier.prepareForUse();

		return new MoaModel(classifier, wrapper);
	}

	public static void write(MoaModel model, OutputStream out)
			throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(out);
		oos.writeObject(model.classifier);
		oos.writeObject(model.wrapper);
	}

	public AbstractClassifier classifier() {
		return classifier;
	}

	public DataInstanceWrapper wrapper() {
		return wrapper;
	}

	public DataInstance wrap(Data data) {
		return this.wrapper.wrap(data);
	}
}