/**
 *
 */
package stream.moa;

import java.io.Serializable;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import moa.classifiers.AbstractClassifier;
import moa.core.InstancesHeader;
import moa.options.OptionHandler;
import stream.Data;
import stream.ProcessContext;
import stream.learner.PredictionService;
import weka.core.Attribute;
import weka.core.Instance;

/**
 * <p>
 * This class is a generic wrapper for MOA classifiers.
 * </p>
 *
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 */
public class MoaClassifier extends MoaProcessor implements PredictionService {

	static Logger log = LoggerFactory.getLogger(MoaClassifier.class);
	protected Object lock = new Object();
	AbstractClassifier moaClassifier;

	protected MoaClassifier() {}

	public MoaClassifier(OptionHandler opt){
		if (opt instanceof AbstractClassifier) {
			moaClassifier = (AbstractClassifier) opt;
			log.debug("Creating new MoaClassifier for class {}", opt.getClass().getName());
		} else
			log.debug("Creating MoaClassifier failed: " + opt.getClass().getName() + "is not an AbstractClassifier");
	}

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);
		moaClassifier.prepareForUse();
		moaClassifier.resetLearning();
	}

	/**
	 * @see stream.moa.MoaProcessor#process(weka.core.Instance)
	 */
	@Override
	public void processInstance(Instance instance) {
		try {
			synchronized (lock) {
				if (moaClassifier.getModelContext()==null) {
					moaClassifier.setModelContext(new InstancesHeader(instance.dataset()));
				}
				moaClassifier.trainOnInstance(instance);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see stream.service.Service#reset()
	 */
	@Override
	public void reset() throws Exception {
		moaClassifier.resetLearning();
	}

	/**
	 * @see stream.learner.PredictionService#getName()
	 */
	@Override
	public String getName() {
		if (id != null)
			return id;
		return moaClassifier.getClass().getCanonicalName();
	}

	/**
	 * @see stream.learner.PredictionService#predict(stream.data.Data)
	 */
	@Override
	public Serializable predict(Data item) throws RemoteException {

		Instance instance = wrap(item);
		double[] votes = new double[0];
		synchronized (lock) {
			votes = moaClassifier.getVotesForInstance(instance);
		}
		if (votes == null || votes.length == 0)
			return null;

		Attribute classAttribute = instance.classAttribute();

		log.debug("Votes: {}", votes);
		int maxIdx = 0;
		for (int i = 0; i < votes.length; i++) {
			String cl = classAttribute.value(i);
			item.put("@prob:" + cl, votes[i]);
			log.debug("prob:{} =  {}", cl, votes[i]);
			if (votes[i] > votes[maxIdx]) {
				maxIdx = i;
			}
		}

		//Regression: votes.length==1 and votes[0]=max=label
		//Numeric class label: maxIndex = label
		//String ClassLabel : maxIndex = index of label
		if (classAttribute.isString() || classAttribute.isNominal()) {
			return instance.classAttribute().value(maxIdx);
		} else {
			if (votes.length==1)
				return votes[0];
			return 0.0+maxIdx;
		}
	}

	@Override
	public double getByteSize() {
		return moaClassifier.measureByteSize();
	}

//    protected AbstractClassifier getClassifier() {
//    	return moaClassifier;
//    }
}