/**
 *
 */
package stream.moa;

import java.util.Map;

import moa.classifiers.AbstractClassifier;
import moa.classifiers.Classifier;
import moa.classifiers.core.driftdetection.ChangeDetector;
import moa.classifiers.drift.DriftDetectionMethodClassifier;
import moa.clusterers.outliers.MyBaseOutlierDetector;
import moa.options.OptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.runtime.setup.ObjectCreator;
import stream.runtime.setup.ParameterInjection;
import stream.util.Variables;

/**
 * @author chris
 * @author roetner
 *
 */
public class MoaObjectFactory implements ObjectCreator {

	static Logger log = LoggerFactory.getLogger(MoaObjectFactory.class);

	/**
	 * @see stream.runtime.setup.ObjectCreator#getNamespace()
	 */
	@Override
	public String getNamespace() {
		return "moa";
	}

	/**
	 * @see stream.runtime.setup.ObjectCreator#create(java.lang.String,
	 *      java.util.Map)
	 */
	@Override
	public Object create(String className, Map<String, String> parameters,
			Variables local) throws Exception {
		log.debug(
				"Creating MOA wrapper object for class '{}', with parameters: {}",
				className, parameters);

		Object o;
		OptionHandler  opt = createPureMoaObject(className, parameters);
		if (opt instanceof AbstractClassifier) {
			if (opt instanceof DriftDetectionMethodClassifier)
				o = new MoaDriftDetectionMethodClassifier(opt);
			else
				o = new MoaClassifier(opt);
		} else if (opt instanceof ChangeDetector) {
			o = new MoaChangeDetector(opt);
		} else if (opt instanceof MyBaseOutlierDetector) {
			o = new MoaOutlierDetector(opt);
		} else
			throw new Exception("MOA class of type " + className
					+ " is currently not supported!");
		ParameterInjection.inject(o,parameters,local);
		log.debug("MOA wrapper is {}", o);
		return o;
	}

	public static OptionHandler createPureMoaObject(String className) throws Exception {
		OptionHandler opt;
		Class<?> clazz = Class.forName(className);
		if (OptionHandler.class.isAssignableFrom(clazz))
			opt = (OptionHandler) clazz.newInstance();
		else
			throw new Exception("MOA class of type " + clazz.getCanonicalName()
					+ " is currently not supported!");
		opt.prepareForUse();
		return opt;
	}

	public static OptionHandler createPureMoaObject(String className, Map<String, String> parameters) throws Exception{
		OptionHandler opt = createPureMoaObject(className);
		MoaParameterFinder.injectParams(parameters, opt);
		opt.prepareForUse();
		return opt;
	}
}
