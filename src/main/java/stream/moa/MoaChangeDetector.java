package stream.moa;

import java.io.Serializable;

import moa.classifiers.core.driftdetection.ChangeDetector;
import moa.options.OptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.Measurable;
import stream.drift.DriftDetectionService;

/**
 * Wrapper for MOA class ChangeDetector.
 *
 * @author roetner
 *
 */
public class MoaChangeDetector extends AbstractProcessor implements DriftDetectionService, Measurable{

	static Logger log = LoggerFactory.getLogger(MoaChangeDetector.class);
	ChangeDetector detector;
	protected String id;
	protected String errorAnnotation = "@error";
	protected String learner;


	public MoaChangeDetector(OptionHandler moaChangeDetector){
		if (moaChangeDetector instanceof ChangeDetector) {
			detector = (ChangeDetector) moaChangeDetector;
			log.debug("Creating new ChangeDetector for class {}", moaChangeDetector.getClass().getName());
		} else
			log.debug("Creating ChangeDetector failed: " + moaChangeDetector.getClass().getName() + "is not a ChangeDetector");
	}

	@Override
	public Data process(Data item) {
		String key = getErrorAnnotation();
		Serializable err = item.get(key);
		try {
			Double error = (Double) err;
			detector.input(error);
		} catch(Exception e) {
			log.error("{} not a double.", errorAnnotation);
		}
		return item;
	}


	/**
	 * @see stream.Measurable#getByteSize()
	 */
	@Override
	public double getByteSize() {
		return detector.measureByteSize();
	}


	@Override
	public void reset() throws Exception {
		detector.resetLearning();
	}


	@Override
	public boolean getDrift() {
		return detector.getChange();
	}


	@Override
	public boolean getWarning() {
		return detector.getWarningZone();
	}

	public String getErrorAnnotation() {
		if (learner==null)
			return errorAnnotation;
		else
			return errorAnnotation+ ":" + learner;
	}

	public void setErrorAnnotation(String errorAnnotation) {
		this.errorAnnotation = errorAnnotation;
	}

	public String getLearner() {
		return learner;
	}

	public void setLearner(String learner) {
		this.learner = learner;
	}

	@Override
	public String getId() {
		if (id != null)
			return id;
		return detector.getClass().getCanonicalName();
	}

	public ChangeDetector getChangeDetector() {
		return detector;
	}
}
