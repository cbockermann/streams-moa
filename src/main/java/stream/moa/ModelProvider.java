/**
 *
 */
package stream.moa;

import java.io.Serializable;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Context;
import stream.Data;
import stream.io.SourceURL;
import stream.learner.PredictionService;
import stream.runtime.ApplicationContext;
import stream.runtime.LifeCycle;
import weka.core.Instance;

/**
 * @author chris
 *
 */
public class ModelProvider implements PredictionService, LifeCycle {

	static Logger log = LoggerFactory.getLogger(ModelProvider.class);
	Object lock = new Object();
	SourceURL model;
	MoaModel classifier;

	/**
	 * @see stream.service.Service#reset()
	 */
	@Override
	public void reset() throws Exception {
	}

	/**
	 * @see stream.learner.PredictionService#getName()
	 */
	@Override
	public String getName() throws RemoteException {
		return null;
	}

	/**
	 * @see stream.learner.PredictionService#predict(stream.Data)
	 */
	@Override
	public Serializable predict(Data item) throws RemoteException {
		Instance instance = this.classifier.wrap(item);
		double[] votes = new double[0];
		synchronized (lock) {
			votes = this.classifier.classifier.getVotesForInstance(instance);
		}
		if (votes == null || votes.length == 0)
			return null;

		log.debug("Votes: {}", votes);
		int maxIdx = 0;
		for (int i = 1; i < votes.length; i++) {
			if (votes[i] > votes[maxIdx]) {
				maxIdx = i;
			}
		}

		return instance.classAttribute().value(maxIdx);
	}


	/**
	 * @see stream.runtime.LifeCycle#init(stream.ApplicationContext)
	 */
	@Override
	public void init(ApplicationContext context) throws Exception {
		log.info("Reading classifier model from {}", model);
		classifier = MoaModel.read(model.openStream());
	}

	/**
	 * @see stream.runtime.LifeCycle#finish()
	 */
	@Override
	public void finish() throws Exception {
	}

	/**
	 * @return the model
	 */
	public SourceURL getUrl() {
		return model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setUrl(SourceURL model) {
		this.model = model;
	}


}
