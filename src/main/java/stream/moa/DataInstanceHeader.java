/**
 *
 */
package stream.moa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import weka.core.Attribute;
import weka.core.Instances;

/**
 * @author chris
 *
 */
public class DataInstanceHeader implements Serializable {

	/** The unique class ID */
	private static final long serialVersionUID = -591073859343592622L;

	ArrayList<Attribute> attributes = new ArrayList<Attribute>();
	ArrayList<String> stringAttributes = new ArrayList<String>();

	int defaultCapacity = 20000;

	public Set<String> keys() {
		Set<String> ks = new LinkedHashSet<String>();
		for (Attribute a : attributes) {
			ks.add(a.name());
		}
		for (String s : stringAttributes) {
			ks.add(s);
		}
		return Collections.unmodifiableSet(ks);
	}

	public void addStringAttribute(Attribute attr) {
		attributes.add(attr);
		stringAttributes.add(attr.name());
	}

	public Attribute getAttribute(int idx) {
		if (idx < 0 || idx >= attributes.size())
			return null;
		return attributes.get(idx);
	}

	public String getKey(int idx) {
		Attribute attr = getAttribute(idx);
		if (attr == null)
			return null;

		return attr.name();
	}

	public Attribute getAttribute(String name) {
		for (Attribute attr : attributes) {
			if (attr.name().equals(name))
				return attr;
		}

		return null;
	}

	public boolean isStringAttribute(String key) {
		return stringAttributes.contains(key);
	}

	/*
	 * As far as I can see this is used by some MOA/Weka learners to access attribute-information while the
	 * defaultCapacity is ignored. If it is used to allocate memory a more thoughtful choice
	 */
	public Instances makeInstances() {
		Instances instances = new Instances("Streams-Set", this.attributes, this.defaultCapacity);
		instances.setClass(getAttribute("@label"));
		return instances;
	}
}
