package stream.moa;

import java.lang.reflect.Field;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import moa.classifiers.AbstractClassifier;
import moa.classifiers.core.driftdetection.ChangeDetector;
import moa.classifiers.drift.DriftDetectionMethodClassifier;
import moa.options.OptionHandler;
import stream.drift.DriftDetectionService;

/**
 * Wrapper for MOA class DriftDetectionMethodClassifier, i.e. a classifier that will
 * be retrained if a concept change is detected. The training set consists of the
 * examples seen since the warn level has been reached.
 *
 * @author roetner
 *
 */
public class MoaDriftDetectionMethodClassifier extends MoaClassifier implements
		DriftDetectionService {

	static Logger log = LoggerFactory.getLogger(MoaDriftDetectionMethodClassifier.class);

	DriftDetectionMethodClassifier classifier;

	public MoaDriftDetectionMethodClassifier(OptionHandler opt) {
		if (opt instanceof DriftDetectionMethodClassifier) {
			classifier = (DriftDetectionMethodClassifier) opt;
			moaClassifier = classifier;
			log.debug("Creating new DriftDetectionMethodClassifier for class {}", opt.getClass().getName());
		} else {
			log.debug("Creating DriftDetectionMethodClassifier failed : {} is not a DriftDetectionMethodClassifier.", opt.getClass().getName());
		}
	}

	public ChangeDetector getChangeDetector() {
	    try {
	    	Class<?> c = classifier.getClass();
			Field detector = c.getDeclaredField("driftDetectionMethod");
			detector.setAccessible(true);
			Object o = detector.get(classifier);
			ChangeDetector cd = (ChangeDetector) o;
			log.debug("retrieved detector {}", o);
			return cd;
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return null;
	}


	public AbstractClassifier getClassifier() {
		return classifier;
	}

	@Override
	public String getId() {
		return getName();
	}

	@Override
	public boolean getDrift() {
		ChangeDetector cd = getChangeDetector();
		if (cd!=null) {
			return cd.getChange();
		}
		return false;
	}

	@Override
	public boolean getWarning() {
		ChangeDetector cd = getChangeDetector();
		if (cd!=null) {
			return cd.getWarningZone();
		}
		return false;
	}

}
