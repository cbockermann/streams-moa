/**
 * 
 */
package stream.moa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Processor;
import weka.core.Attribute;

/**
 * @author chris
 * 
 */
public class CreateInstance implements Processor {

	static Logger log = LoggerFactory.getLogger(CreateInstance.class);

	String label = "@label";
	String[] features;

	String key = "@instance";

	DataInstanceWrapper factory = new DataInstanceWrapper();

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		DataInstance instance = factory.wrap(input);

		log.info("Instance: {}", instance);

		log.info("Instance has {} attributes.", instance.numAttributes());
		log.info("Class attribute has index {}",
				instance.header.attributes.indexOf(instance.classAttribute()));
		log.info("Class index is: {}", instance.classIndex());

		StringBuffer kr = new StringBuffer();
		StringBuffer vr = new StringBuffer();

		for (int a = 0; a < instance.numAttributes(); a++) {
			Attribute attr = instance.attribute(a);

			kr.append(attr.name());
			vr.append(instance.value(a));

			if (a + 1 < instance.numAttributes()) {
				kr.append(", ");
				vr.append(", ");
			}
		}

		log.info("Values:\n\t{}\n\t{}", kr.toString(), vr.toString());

		input.put(key, instance);

		return input;
	}
}
