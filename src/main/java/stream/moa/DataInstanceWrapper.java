/**
 *
 */
package stream.moa;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Keys;
import weka.core.Attribute;

/**
 * This class implements the wrapping of data items to Weka instances. For
 * wrapping, it follows a strict set of conventions, which are:
 *
 * <ol>
 * <li>No keys starting with @ are mapped to the instance, <b>except</b> the
 * <code>@label</code> attribute.</li>
 * <li>Any key, which has a first value that can be cast to Number, is mapped to
 * numerical attributes, all others are regarded as String attributes.</li>
 * <li>The order of attributes in the instance is: class attribute first, then
 * all attributes in the order of appearance in the <b>first</b> mapped data
 * item.</li>
 * </ol>
 *
 * @author Christian Bockermann
 *
 */
public class DataInstanceWrapper implements Serializable {

	/** The unique class ID */
	private static final long serialVersionUID = -893141982322824870L;

	static Logger log = LoggerFactory.getLogger(DataInstanceWrapper.class);

	final DataInstanceHeader header = new DataInstanceHeader();

	final String labelKey;
	final String[] features;
	final String[] labelValues;

	public DataInstanceWrapper() {
		labelKey = "@label";
		features = new String[] { "*" };
		this.labelValues = null;
	}

	public DataInstanceWrapper(String labelKey, String[] features,
			String[] labelValues) {
		this.labelKey = labelKey;
		this.features = features;
		this.labelValues = labelValues;
	}

	/**
	 * This method initializes the mapping of items to instances. The mapping is
	 * based on the <i>first</i> item that is observed.
	 *
	 * @param item
	 *            The item the mapping is initialized from.
	 */
	protected void init(Data item) {

		Attribute label = null;

		if (!header.attributes.isEmpty()) {
			log.warn("DataInstanceWrapper already initialized!");
			return;
		}

		if (!item.containsKey(labelKey)) {
			log.warn("Item does not provide label key '{}': {}", labelKey, item);
			log.warn("Cannot initialize instance factory...");
			return;
		}

		if (labelValues != null && labelValues.length > 0) {
			//
			// special case: class label values have been provided by the user.
			//
//			label = new StringAttribute(labelKey, labelValues[0]);
//			for (int i = 1; i < labelValues.length; i++) {
//				label.addStringValue(labelValues[i]);
//			}
			label = new Attribute(labelKey, Arrays.asList(labelValues));
			// header.attributes.add(label);
		} else {
			if (isNumerical(labelKey, item)) {
				header.attributes.add(new Attribute(labelKey));
				log.debug("Creating new Numeric attribute for label '{}'",
						labelKey);
			} else {
				String labelValue = item.get(labelKey) + "";
				label = new StringAttribute(labelKey, labelValue);

				if (item.containsKey("@classes")) {
					try {
						String[] classes = (String[]) item.get("@classes");
						log.info("Found class labels header '@classes': {}",
								Arrays.asList(classes));

//						label = new StringAttribute(labelKey, classes[0]);
//						for (int i = 1; i < classes.length; i++) {
//							label.addStringValue(classes[i]);
//						}
						label = new Attribute(labelKey, Arrays.asList(labelValues));

					} catch (Exception e) {
						log.error("Failed to add class label: {}",
								e.getMessage());
					}
				} else {
					label = new StringAttribute(labelKey, labelValue);
				}

				// header.attributes.add(label);
			}
		}

		for (String key : Keys.select(item, features)) {

			if (header.keys().contains(key)) {
				log.info("Skipping key '{}' - already included.", key);
				continue;
			}

			if (key.startsWith("@")) {
				continue;
			}

			if (isNumerical(key, item)) {
				header.attributes.add(new Attribute(key));
				log.info("Added numeric attribute '{}'", key);
			} else {
				Serializable value = item.get(key);
				Attribute attr = new Attribute(key, (List<String>) null);

				int idx = attr.addStringValue(value.toString());
				log.info("Added string attribute, with first index '{}' => {}",
						value.toString(), idx);
			}
		}

		if (label != null) {
			header.attributes.add(label);
		}
	}

	public DataInstance wrap(Data item) {

		// initialize header based on first data item.
		//
		if (header.attributes.isEmpty()) {
			init(item);
		}

		for (Attribute attr : header.attributes) {

			Serializable value = item.get(attr.name());
			if (value == null) {
				log.warn("Missing value for attribute '{}'!", attr.name());
				continue;
			}

			if (!attr.isNumeric()) {
				int idx = attr.indexOfValue(value.toString());
				if (idx < 0) {
					idx = attr.addStringValue(value.toString());
				}
			}
		}

		return new DataInstance(item, header);
	}

	public boolean isNumerical(String key, Data item) {
		Serializable value = item.get(key);

		if (value != null && Number.class.isAssignableFrom(value.getClass())) {
			return true;
		}

		return false;
	}

	public boolean isString(String key, Data item) {
		return !isNumerical(key, item);
	}
}
