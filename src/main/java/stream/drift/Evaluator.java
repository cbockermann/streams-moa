package stream.drift;

import java.lang.reflect.Field;

import moa.classifiers.AbstractClassifier;
import moa.classifiers.Classifier;
import moa.classifiers.drift.DriftDetectionMethodClassifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.moa.MoaDriftDetectionMethodClassifier;

public class Evaluator extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(Evaluator.class);

	protected DriftDetectionService detector;

	//	protected String keyChangeGT = "@changeGT";
	protected String keyDetected = "@detected";
	protected String keyWarning = "@warning";

	protected Long counter = 0L;

	@Override
	public Data process(Data item) {

		counter++;
		boolean isDetected = detector.getDrift();
		boolean isWarning = detector.getWarning();

		item.put(keyDetected,(isDetected ? 1 : 0));
		item.put(keyWarning,(isWarning ? 1 : 0));

		if (isDetected) {
			log.info("Detection of Concept Drift at item {}", counter);
		}
		return item;
	}

	public DriftDetectionService getDetector() {
		return detector;
	}

	public void setDetector(DriftDetectionService detector) {
		this.detector = detector;
	}

	public String getKeyDetected() {
		return keyDetected;
	}

	public void setKeyDetected(String keyDetected) {
		this.keyDetected = keyDetected;
	}

	public String getKeyWarning() {
		return keyWarning;
	}

	public void setKeyWarning(String keyWarning) {
		this.keyWarning = keyWarning;
	}

	@Override
	public void finish() throws Exception {
	}


}
