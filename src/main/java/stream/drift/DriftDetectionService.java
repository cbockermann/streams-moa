package stream.drift;

import moa.classifiers.core.driftdetection.ChangeDetector;
import stream.service.Service;

/**
 * Service that provides anytime information on wether a concept drift has occured.
 * @author roetner
 *
 */
public interface DriftDetectionService extends Service {

	public boolean getDrift();
	public boolean getWarning();
	public String getId();

	public ChangeDetector getChangeDetector();
	/*
	 * These values might be interesting for monitoring and optimization
	 * of the change detecor's parameters and where used for my Bachelor
	 * thesis. The values can be calculated from private fields of SOME
	 * changeDetectors.
	 *
	public double getPValueDrift();
	public double getPValueWarn();
	public double getTValue();
	*/
}
