\subsection{\label{sec:streams}Modelling Data Flows with the \textsf{streams} Framework}
Modern data processing chains are generally presented by their data flow. In such data flow
graphs, a source emitting a sequence of records is linked to a graph of nodes, each of which
provides the processing of input and the delivery of some output. Such data flow graphs are
inherent to all modern data processing engines, especially in the field of data stream processing.

\begin{figure}[h!]
\centering
\begin{tikzpicture}[scale=0.75,transform shape]
\node[rectangle, rounded corners, fill=black!10, draw=black!60, minimum width=8cm, minimum height=3.6cm] (A) at (0,0) {};
\node[anchor=west] at (-4,1.6) {\scriptsize{\ttfamily <application>}};
\node[anchor=west] at (-4,-1.6) {\scriptsize{\ttfamily </application>}};

\node[stream,scale=0.7] (S) at (-2.75,0.75) {};
\node at (-2.75,0.25) {\scriptsize{\ttfamily <stream/>}};

\node[process',scale=0.7] (P1) at (-1,0.75) {};
\node at (-1,0.25) {\scriptsize{\ttfamily <process/>}};

\node[stream,scale=0.7] (Q) at (0,-0.5) {};
\node at (0,-1) {\scriptsize{\ttfamily <queue/>}};

\node[process',scale=0.7] (P2) at (2,0.75) {};
\node at (2,0.25) {\scriptsize{\ttfamily <process/>}};

\node[process',scale=0.7] (P3) at (2,-0.5) {};
\node at (2,-1) {\scriptsize{\ttfamily <process/>}};

\draw[edge] (S) -- (P1);
\draw[edge,fill=none] (P1) -- (0,0.75) -- (Q);
\draw[edge,fill=none] (Q) -- (1,0.75) -- (P2);
\draw[edge] (Q) -- (P3);

\end{tikzpicture}
\caption{\label{fig:streamingApp}The outline of an application in \textsf{streams} -- a graph of
connected processes, modelled using XML. Every basic element of the data flow has a corresponding XML element.
}
\end{figure}



With the \textsf{streams} framework we aim at finding an appropriate level of abstraction
that allows for the design of data flows independent of a specific execution engine. The
focus of \textsf{streams} is a light-weight middle-layer API in combination with a descriptive
XML-based specification language, that can directly be executed with the \textsf{streams}
runtime or be mapped to topologies of the Apache Storm engine. The predominant focus in the
development of \textsf{streams} was a simplistic light-weight API and process definition that
is easily applicable by domain experts and adaptable to a variety of different use cases.

Each of the connected processes in \textsf{streams} consists of a pipeline of user-functions,
which are applied to the processed items in order. Figure \ref{fig:processorPipeline} shows a
brick-like visualization of a process as pipeline of functions. The source nodes provides a
sequence of data items that is individually processed by the process pipeline of user-functions.
These user-functions are typically implemented in single Java classes and are directly referenced 
by their implementation name from inside the XML element of the corresponding process. 
% By aiming at this user-function level of granularity, the process designer is able to move 
% computational intensive tasks to separate processes that may execute in parallel. 
The figure shows functions for the calibration, image cleaning and extraction of Hillas parameters. 
With these features available, a classifier model can be applied.

% \vspace{-2ex}
\begin{figure}
\centering
\begin{tikzpicture}[scale=0.6,transform shape]
\input{figures/processor-pipeline}
\end{tikzpicture}
\caption{\label{fig:processorPipeline}A data stream {\em source} connected to a process
that consists of four different user functions for calibration and feature extraction.}
\end{figure}

To achive the maximum level of flexibility, the data items (or tuples) that are passed from one
user-function to the next, are wrapped in a simple hashmap (or dictionary), that can be accessed 
and enriched by each user-function. The data that can be stored in each of these can be of arbitrary
serializable types, allowing for the implementation of user-functions that work on simple data
types as well as frames/images (video processing) or telescope event data as we will discuss in
the next section.
To further ease the modelling of data flows with a set of implemented user-functions, the \textsf{streams}
approach facilitates an automatic mapping of XML attributes to classes following the JavaBean conventions.
This removes any intermediate layer between process modelling and function implementation.

% \vspace{-4ex}
\begin{figure}[h!]
\centering
\begin{tikzpicture}[scale=0.85,transform shape]
\node at (0,0) {
\begin{lstlisting}[basicstyle=\ttfamily\footnotesize]
  <process input="telescope:data">
    <fact.data.DrsCalibration calibrationFile="file:/data/calib.fits" />
    <fact.image.ImageCleaning energyThreshold="2.45" />
    <fact.image.features.HillasParameters />
    <streams.weka.Apply modelUrl="http://sfb876.de/rforest.weka" />
  </process>
\end{lstlisting}
};
\end{tikzpicture}
  \caption{\label{fig:csvStream}The XML correspoding to the pipeline of the previous figure.}
\end{figure}