\begin{abstract}
The use of machine learning has become ubiquituous in a vast amount of different
application domains. Ranging from large scale Big Data analysis to smartphones or
embedded devices -- machine learning methods contribute to various aspects such as
smartphone energy savings, efficient traffic-aware trip planning or optimization
of productions in smart factories.

Within the last decade, the latency from data acquisition to results required from
machine learning has shrinked to near-realtime requirements, which demands for
online learning and application of prediction models and estimators in most of these
settings.

In this work we present the integration of the MOA online machine learning library
into the highly flexible \textsf{streams} framework. This combination allows for
rapidly designing machine learning dataflows for various platforms. We demonstrate
the usefulness of this integrated approach on several real-world examples.
\end{abstract}


\section{\label{sec:intro}Introduction}
The processing of streaming data is becoming a key quality of modern analysis
platforms: The continuous decreasing latency, from data acquisition to data analysis
challenges existing analytical infrastructures. Iterative batch jobs may allow for
massive data processing, but are too sedate to meet todays requirements. Decisions
need to be available as fast as possible, requiring real-time predictions that are
made based on the latest available data and up-to-date models.

As an example, we may look at soccer player data: the decision, when to substitute
an exhausted player may be crucial to the game outcome. In the DEBS challenge 2013 \cite{DEBS},
the location data of soccer players is gathered in real-time with a high resolution.
Keeping track of that data may provide insights to the degree of exhaustion of a 
player or allow for the comparison of the player with its direct opponent on the field.

A different example comes from traffic organization: given the state of the current
traffic situation in an area may provide valuable insights and and allow for a
real-time optimization of the traffic flow. With accurate predictions of where new
jams will occur in the near future -- based on an up-to-date prediction model -- these
jams might be avoidable, saving people's time and overall energy consumption.

\smallskip

All these examples do require an analytical data-flow that allows for the online
processing of real-time sensor data, the extraction of required features and the
updating and application of machine learning models to reveal in-time predictions.
In this work, we propose a unified framework for modelling such data-flows in a
declarative manner, allowing for an easy adaption of the techniques to a wide range
of use-cases. We present the integration of our declarative \textsf{streams} framework
with the MOA online learning library. This combination allows for the integration of
state-of-the-art machine learning approaches into various data analysis chains.

\smallskip

The paper is structured as follows: In Section \ref{sec:RelatedWork} we review the
landscape of existing data-flow engines and algorithms for online learning. Following
that, we outline the concepts of our \textsf{streams} framework for modelling such
data flows and the MOA machine learning library, prodiving training and evaluation of
high-end classifier methods. Here, we also describe our integration of MOA into the
\textsf{streams} framework and the prototyping environment, that results from that
integration.
We demonstrate the power of our approach using the real-time soccer data provided in
the DEBS 2013 challenge \cite{DEBS} in Section \ref{sec:Experiments} and conclude
in Section \ref{sec:Conclusion}.
