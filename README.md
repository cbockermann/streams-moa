# streams-moa #

This repository extends the [``streams`` framework](https://bitbucket.org/cbockermann/streams) and is based upon the bachelor thesis by Stefan Roetner [Behandlung von Concept Drift in zyklischen Prozessen](http://www-ai.cs.uni-dortmund.de/PublicPublicationFiles/roetner_2014a.pdf). 
You can find some more details about the implementation and the concept behind ``streams-moa``.


Example usage of MOA in ``streams`` environment:


```xml
<moa.classifiers.drift.DriftDetectionMethodClassifier id="ddm"
	driftDetectionMethod="DDM -n 100"
	baseLearner="meta.WEKAClassifier -l weka.classifiers.trees.J48"
	minNumInstances="1000"
/>
``` 

All you need is to add this dependency to your maven project:

```xml
	<dependency>
		<groupId>de.sfb876</groupId>
		<artifactId>streams-moa</artifactId>
		<version>0.1.2</version>
		<scope>compile</scope>
	</dependency>
```


## Examples

#### Online prediction

1. Define prediction services listening for learners 'HT' and 'NB'
2. Perform evaluation
3. Learn a Hoeffding Tree 'HT' and Naive Bayes 'NB'
4. Compute statistics

```xml
<container id="moa-test" server="false" >

	<stream id="stream" class="stream.io.CsvStream" separator=";"
		url="classpath:/multi-golf.csv" limit="100"/>

	<process input="stream">

		<RenameKey from="play" to="@label" />
		<RemoveKeys keys="@stream"/>

		<stream.learner.Prediction learner="HT" />
		<stream.learner.Prediction learner="NB" />
		<stream.learner.evaluation.PredictionError />

		<moa.classifiers.trees.HoeffdingTree id="HT" binarySplits="true" />
		<moa.classifiers.bayes.NaiveBayes id="NB"/>

		<stream.statistics.Sum keys="@error:NB" />
		<stream.statistics.Sum keys="@error:HT" />

		<PrintData />

	</process>
</container>
```

#### Concept drift detection

1. Define prediction service listening for learner 'NB'
1. Define concept drift detection service listening for 'ddmClassifier' based on Hoeffding Tree
2. Perform evaluation
3. Train a Hoeffding Tree for concept drift detection 'ddmClassifier' and Naive Bayes 'NB'
4. Compute statistics
```xml
<container id="moa-test" server="false" >

	<stream id="stream" class="stream.io.CsvStream" separator=","
		url="classpath:/conceptDriftData.csv" />

	<process input="stream">

		<stream.learner.Prediction learner="NB" />
		<stream.learner.Prediction learner="ddmClassifier" />
		<stream.learner.evaluation.PredictionError />

		<stream.drift.Evaluator detector="ddm" />
		<stream.drift.Evaluator detector="ddmClassifier" />

		<moa.classifiers.drift.DriftDetectionMethodClassifier id="ddmClassifier"
			baseLearner="trees.HoeffdingTree -b" driftDetectionMethod="DDM -n 100" />
		<moa.classifiers.bayes.NaiveBayes id="NB"/>

		<stream.statistics.Sum keys="@error:ddmClassifier" />
		<stream.statistics.Sum keys="@error:NB" />

		<moa.classifiers.core.driftdetection.DDM id="ddm" minNumInstances="100" learner="NB"/>
	</process>
</container>
```


#### Outlier detection

```xml
<container id="moa-outlier-test">
	<stream id="outlierStream" class="stream.io.CsvStream" separator=","
		url="classpath:/conceptDriftData.csv" />
	<process input="outlierStream">

		<moa.clusterers.outliers.AbstractC.AbstractC id="AbstractC" windowSize="200"/>
		<moa.clusterers.outliers.Angiulli.ApproxSTORM id="ApproxSTORM" windowSize="200"/>
		<moa.clusterers.outliers.Angiulli.ExactSTORM id="ExactSTORM" windowSize="200"/>
		<moa.clusterers.outliers.SimpleCOD.SimpleCOD id="SimpleCOD" windowSize="200"/>
		<moa.clusterers.outliers.MCOD.MCOD id="MCOD" windowSize="200"/>

	</process>
</container>
```